ARG ALPINE_VERSION=3.17
ARG THTTPD_VERSION=2.29

FROM alpine:${ALPINE_VERSION} as builder

RUN apk --no-cache add build-base

ARG THTTPD_VERSION

RUN wget "http://www.acme.com/software/thttpd/thttpd-${THTTPD_VERSION}.tar.gz" \
	&& tar xf "thttpd-${THTTPD_VERSION}.tar.gz" \
	&& mv "thttpd-${THTTPD_VERSION}" /thttpd

WORKDIR /thttpd

RUN ./configure && make CCOPT='-Ofast -s -static' thttpd

RUN adduser -D static


FROM scratch

EXPOSE 3000

COPY --from=builder /etc/passwd /etc/passwd

COPY --from=builder /thttpd/thttpd /

USER static
WORKDIR /home/static

CMD ["/thttpd", "-D", "-h", "0.0.0.0", "-p", "3000", "-d", "/home/static", "-u", "static", "-l", "-", "-M", "60"]
